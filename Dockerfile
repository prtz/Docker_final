FROM php:7.2-fpm-buster

# Install extensions
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libpq-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && mkdir -p /speedtest/results/ \
#    && chown www-data:www-data /speedtest/results/ \
    && docker-php-ext-install pdo_pgsql

# Copy sources

COPY backend/ /speedtest/backend
COPY --chown=www-data:www-data results/*.php results/*.ttf /speedtest/results/
COPY *.js docker/*.php /speedtest/
COPY docker/entrypoint.sh /

EXPOSE 9000
WORKDIR /var/www/html
CMD ["bash", "/entrypoint.sh"]
