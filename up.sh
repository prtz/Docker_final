#!/bin/bash

sudo docker network create dkr-net
sudo docker-compose -p rbmdkrfinalefk -f efk.compose.yml up -d --force-recreate
sudo docker-compose -p rbmdkrfinalapp -f app.compose.yml up -d --force-recreate
#sudo docker exec -i rbmdkrfinalapp-db-1 /bin/bash -c "PGPASSWORD=youseenothing psql --username postgres postgres" < /app/telemetry_postgresql.sql
sleep 5
sudo docker exec -i rbmdkrfinalapp-db-1 sh -c "sh sql_add_telemeetry.sh"
sudo docker exec -i rbmdkrfinalapp-app-1 sh -c "chown -R www-data:www-data /var/www/html/results"